from jnpr.junos import Device
from jnpr.junos import exception as jexception
from lxml import etree
from flask import Flask, jsonify, request, session, render_template, url_for, redirect, flash
from flask.ext import restful
from jinja2 import evalcontextfilter, Markup
from datetime import datetime
from functools import wraps
from contextlib import contextmanager
import xmltodict
import json

app = Flask(__name__)
app.secret_key = 'chassisfun'
api = restful.Api(app)


@app.template_filter()
@evalcontextfilter
def humanize(eval_ctx, unixdate):
    """ Jinja2 filter to format a unix date """

    formatted = str(datetime.fromtimestamp(int(unixdate)).strftime('%Y-%m-%d %H:%M:%S'))
    if eval_ctx.autoescape:
        formatted = Markup(formatted)
    return formatted

class MyDevice(Device):
    """ Device wrapper to allow context manager """
    
    def __enter__(self, *args, **kvargs):
        return self.open(*args, **kvargs)

    def __exit__(self, *args, **kvargs):
        self.close()

def get_hardware(user,password,host):
    """ 
    Method to get json string of hardware in switch 
    Equivalant to 'show chassis hardware' command
    
    """

    creds = {'user':user,
             'password':password,
             'host':host}

    with MyDevice(**creds) as dev:
        inv = dev.rpc.get_chassis_inventory()
        inv = json.loads(json.dumps(xmltodict.parse(etree.tostring(inv))))
        return inv

def authenticated(f):
    """ Authentication wrapper for "protected" methods """

    @wraps(f)
    def decorated(*args, **kwargs):
        if not session.get('auth', None):
            return redirect(url_for('index'))
        return f(*args, **kwargs)
    return decorated


@app.route('/', methods=['GET', 'POST'])
def index():
    """ Handles log in """
    
    if request.method == 'POST':
        if not request.form['user'] or not request.form['password']:
            flash('User and Password required')
        else:
            auth = {'user':request.form['user'],'password':request.form['password']}
            session['auth'] = auth
            return redirect(url_for('hardware'))

    return render_template('index.html', authed=False)
    
@app.route('/inventory', methods=['GET','POST'])
@authenticated
def hardware():
    """ 
    Handles form POST with host
    and redirects to hardware_inv  
    
    """
    if request.method == 'POST':
        host = request.form['host']
        if host:
            return redirect(url_for('hardware_inv', host=host))

        flash('Host IP required')

    return render_template('hardware.html', raw=False, authed=True)

@app.route('/inventory/<host>')
@authenticated
def hardware_inv(host):
    """ 
    Finds hardware of given host 
    handles basic errors with a flashed message
    
    """
    
    auth = session['auth']
    try:
        inv = get_hardware(host=host, **auth)
        return render_template('hardware.html', raw=json.dumps(inv), authed=True)
    except jexception.ConnectUnknownHostError, e:
        flash("Invalid Host")
        return redirect(url_for('hardware'))
    except jexception.ConnectRefusedError, e:
        flash("Connection Refused")
        return redirect(url_for('hardware'))
    except jexception.ConnectTimeoutError, e:
        flash("Connection Timeout")
        return redirect(url_for('hardware'))
    except jexception.ConnectAuthError, e:
        flash("Invalid Username or Password")        
        return redirect(url_for('logout'))
    

@app.route('/logout')
def logout():
    """ Logs out """

    session.clear()
    return redirect(url_for('index'))

class JuniperHardware(restful.Resource):
    """ 
    flask-restful class as a proof of concept api

    """

    def get(self, user, password, host):
        r = get_hardware(user,password,host)
        return jsonify(results=r)

api.add_resource(JuniperHardware, '/api/<string:user>/<string:password>/<string:host>')

if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0')
