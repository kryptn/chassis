I needed to install some dependancies for ncclient:  
    libxml2-dev libxslt1-dev python-dev python-lxml

clone the repo

    virtualenv venv
    source venv/bin/activate
    pip install -r requirements.txt
    python app.py
